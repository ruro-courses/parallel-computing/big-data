from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np

Ns = np.array([1, 2, 4, 8, 16, 32])
Ts = np.array([
    [
        # baseline
        423887079944,
        387640568142,
        375915017057,
        389079452853,
    ],
    [
        # N=2
        243074502654,
        222389530489,
        226088658610,
        315497250195,
    ],
    [
        # N=4
        245408904615,  
        176127045575,  
        163290689020,  
        151990486190,  
    ],    
    [    
        # N=8    
        119804040179,  
        116844844608,  
        180399415048,  
        116659082245,  
    ],    
    [    
        # N=16    
        99756268667,
        121331040056,
        198211212918,
        135630370925,
    ], 
    [
        # N=32    
        93886883023,
        93886883023,
        93886883023,
        93886883023,
    ]
])
Cs = np.array([
    [
        # Baseline (combiner)
        284385862702,
        274776568697,
    ],
    [
        # N=2 (combiner)
        155149017777,
        158083139206,
    ],
    [
        # N=4 (combiner)
        130542540615,
        144379556472,
    ],
    [
        # N=8 (combiner)
        116914779785,
        115085660445,
    ],
    [
        # N=16 (combiner)
        124438640624,
        105338439567,
    ],
    [
        # N=32 (combiner)
        94604171689,
        94604171689,
    ],
])
Tmins = Ts.min(axis=-1)
Cmins = Cs.min(axis=-1)
Tbase = Tmins[0]
Cbase = Cmins[0]

Ax = np.linspace(1, 32, 100)
Talpha = (Tmins[-1]/Tbase - 1/Ns[-1])/(1-1/Ns[-1])
Talpha = Talpha.round(2)
TAy = 1/(Talpha + (1-Talpha)/Ax)
Calpha = (Cmins[-1]/Cbase - 1/Ns[-1])/(1-1/Ns[-1])
Calpha = Calpha.round(2)
CAy = 1/(Calpha + (1-Calpha)/Ax)

sns.set_theme()
fig, ax = plt.subplots(figsize=(10.8, 10.8), tight_layout=True)
ax.set_aspect(1)
ax.set_xticks(1 + 2*np.arange(16))
ax.set_yticks(1 + 2*np.arange(16))
eps = 0.1
ax.set_xlim(1-eps, 32+eps)
ax.set_ylim(1-eps, 32+eps)
ax.set_xlabel("Num. Reducers")
ax.set_ylabel("Speedup")
ax.plot(Ns, Ns, color="C0", linestyle=":", label="Theoretical Maximum (Regular)")
ax.plot(Ns, Tbase/Cbase * Ns, color="C1", linestyle=":", label="Theoretical Maximum (With Combiner)")
if True:
    ax.plot(Ax, TAy, color="C0", lw=2, label=fr"Amdahl's law ($\alpha={Talpha}$)")
    ax.plot(Ax, Tbase/Cbase * CAy, lw=2, color="C1", label=fr"Amdahl's law ($\alpha={Calpha}$)")
else:
    ax.plot(Ns, Tbase/Tmins, lw=2, color="C0", label="Regular")
    ax.plot(Ns, Tbase/Cmins, lw=2, color="C1", label="With Combiner")
ax.scatter(Ns[:, None].repeat(4, axis=-1), Tbase/Ts, s=12, color="C0")
ax.scatter(Ns[:, None].repeat(2, axis=-1), Tbase/Cs, s=12, color="C1")
ax.legend()
fig.savefig("plot.png")
