#### Компиляция программы

```sh
mvn clean package
```

#### Запуск программы

Например, вот так:
```sh
hadoop jar target/candle.jar candle \
    -Dcandle.remove.output=true \
    -Dcandle.verbose=true \
    -Dcandle.date.from=20110111 \
    -Dcandle.date.to=20110112 \
    -Dcandle.securities='SVH1' \
    -Dcandle.num.reducers=10 \
    data/test_sample \
    output
```

#### Отчет

[Ссылка](https://docs.google.com/document/d/1Iojm4Tzap-OKO8FDgNF6ukUilROpOUbbzlYSczPiH_Q/edit?usp=sharing)
