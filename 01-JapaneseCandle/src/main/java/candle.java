import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

// region WritableComparable container types
class CandleSymbolID implements WritableComparable<CandleSymbolID> {
    public final LongWritable candle = new LongWritable();
    public final Text symbol = new Text();
    public final LongWritable id = new LongWritable();

    public CandleSymbolID() {}

    public CandleSymbolID(long candle_, String symbol_, long id_) {set(candle_, symbol_, id_);}

    public void set(long candle_, String symbol_, long id_) {
        candle.set(candle_);
        symbol.set(symbol_);
        id.set(id_);
    }

    @Override
    public int compareTo(CandleSymbolID other) {
        int ret;
        if ((ret = candle.compareTo(other.candle)) != 0) return ret;
        if ((ret = symbol.compareTo(other.symbol)) != 0) return ret;
        return id.compareTo(other.id);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        candle.write(out);
        symbol.write(out);
        id.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        candle.readFields(in);
        symbol.readFields(in);
        id.readFields(in);
    }
}

class Deal extends DoubleWritable {
    Deal() {}

    Deal(double f) {super(f);}
}
// endregion

public class candle {
    // region Constants
    private static final String date_format_string = "yyyyMMdd";
    private static final SimpleDateFormat date_format = new SimpleDateFormat(date_format_string);
    private static final String time_format_string = "kkmm";
    private static final SimpleDateFormat time_format = new SimpleDateFormat(time_format_string);
    private static final String datetime_format_string = date_format_string + time_format_string + "ssSSS";
    private static final SimpleDateFormat datetime_format = new SimpleDateFormat(datetime_format_string);
    private static final String price_format = "%.1f";
    // endregion

    // region Globals
    private static int num_reducers;
    private static boolean verbose;
    private static boolean force_file_order;
    private static boolean remove_output;
    private static boolean use_combiner;
    private static Pattern symbol_pattern;
    private static long candle_width;
    private static long date_from;
    private static long date_to;
    private static long daily_candle_from;
    private static long daily_candle_to;
    private static long candles_per_day;
    // endregion

    // region Initialize global state from Configuration
    public static void init_state(Configuration conf) {
        // Boolean options
        verbose = conf.getBoolean("candle.verbose", true);
        force_file_order = conf.getBoolean("candle.force.file.order", false);
        remove_output = conf.getBoolean("candle.remove.output", true);
        use_combiner = conf.getBoolean("candle.use.combiner", false);

        // Symbol, Candle Width, Num Reducers
        symbol_pattern = Pattern.compile(conf.get("candle.securities", ".*"));
        candle_width = conf.getLong("candle.width", 300000);
        num_reducers = conf.getInt("candle.num.reducers", 1);
        check_state(candle_width >= 1, "`width` must be at least 1");
        check_state(num_reducers >= 1, "`num.reducers` must be at least 1");

        // Date
        date_from = parse_datetime_or_die(date_format, conf.get("candle.date.from", "19000101"));
        date_to = parse_datetime_or_die(date_format, conf.get("candle.date.to", "20200101"));
        check_state(date_from < date_to, "`date.from` should be before `date.to`");

        // Time
        long time_from = parse_datetime_or_die(time_format, conf.get("candle.time.from", "1000"));
        long time_to = parse_datetime_or_die(time_format, conf.get("candle.time.to", "1800"));
        check_state(time_from < time_to, "`time.from` should be before `time.to`");
        check_state(time_from % candle_width == 0, "`time.from` must be divisible by `width`");
        check_state(time_to % candle_width == 0, "`time.to` must be divisible by `width`");
        daily_candle_from = time_from / candle_width;
        daily_candle_to = time_to / candle_width;
        candles_per_day = TimeUnit.DAYS.toMillis(1) / candle_width;
    }
    // endregion

    // region Helper Functions
    public static long parse_datetime_or_die(SimpleDateFormat fmt, String str) {
        try {return fmt.parse(str).getTime();} catch (ParseException e) {
            throw new IllegalStateException("Date/Time doesn't match the format", e);
        }
    }

    public static String long_to_datetime_str(long data) {
        return datetime_format.format(new Date(data));
    }

    public static String double_to_price_str(double price) {
        return String.format(price_format, price);
    }

    public static void check_state(boolean ok, String message) {
        if (!ok) throw new IllegalStateException(message);
    }
    // endregion

    // region Mapper
    public static class ParseCSVMapper extends Mapper<Object, Text, CandleSymbolID, Deal> {
        private static final String header = "#SYMBOL,SYSTEM,MOMENT,ID_DEAL,PRICE_DEAL,VOLUME,OPEN_POS,DIRECTION";

        @Override
        protected void setup(
            Mapper<Object, Text, CandleSymbolID, Deal>.Context context
        ) throws IOException, InterruptedException {
            super.setup(context);
            init_state(context.getConfiguration());
        }

        @Override
        public void map(
            Object in_key,
            Text in_val,
            Context context
        ) throws IOException, InterruptedException {
            String in_val_str = in_val.toString();
            if (!header.equals(in_val_str)) {
                String[] array = in_val.toString().split(",");
                check_state(array.length == 8, "Input CSV has wrong number of columns");

                // Symbol doesn't match regex
                String symbol = array[0];
                if (!symbol_pattern.matcher(symbol).matches())
                    return;

                // Moment is outside the target date range
                long moment = parse_datetime_or_die(datetime_format, array[2]);
                if (!(date_from <= moment && moment < date_to))
                    return;

                // Candle is outside the target time range
                long candle = moment / candle_width;
                long daily_candle = candle % candles_per_day;
                if (!(daily_candle_from <= daily_candle && daily_candle < daily_candle_to))
                    return;

                long id = Long.parseLong(array[3]);
                double deal = Double.parseDouble(array[4]);
                context.write(
                    new CandleSymbolID(candle, symbol, id),
                    new Deal(deal)
                );
            }
        }
    }
    // endregion

    // region Partitioners and Grouper (Secondary Key Comparisons)
    public static class SymbolPartitioner extends Partitioner<CandleSymbolID, Deal> {
        @Override
        public int getPartition(
            CandleSymbolID in_key,
            Deal in_val,
            int num_partitions
        ) {return in_key.symbol.hashCode() % num_partitions;}
    }

    public static class SymbolCandlePartitioner extends Partitioner<CandleSymbolID, Deal> {
        @Override
        public int getPartition(
            CandleSymbolID in_key,
            Deal in_val,
            int num_partitions
        ) {return (in_key.symbol.hashCode() + in_key.candle.hashCode()) % num_partitions;}
    }

    public static class CandleSymbolGrouper extends WritableComparator {
        public CandleSymbolGrouper() {super(CandleSymbolID.class, true);}

        @Override
        public int compare(WritableComparable a, WritableComparable b) {
            CandleSymbolID a_ = (CandleSymbolID) a;
            CandleSymbolID b_ = (CandleSymbolID) b;

            // Only compare keys 1 and 2 (Candle and Symbol) when grouping
            // Ignore key 3 (ID), resulting in all IDs getting the same group
            int r = a_.candle.compareTo(b_.candle);
            if (r != 0) return r;
            return a_.symbol.compareTo(b_.symbol);
        }
    }
    // endregion

    // region Combiner
    public static class CandleDealCombiner extends Reducer<CandleSymbolID, Deal, CandleSymbolID, Deal> {
        @Override
        protected void setup(
            Reducer<CandleSymbolID, Deal, CandleSymbolID, Deal>.Context context
        ) throws IOException, InterruptedException {
            super.setup(context);
            init_state(context.getConfiguration());
        }

        @Override
        protected void reduce(
            CandleSymbolID in_key,
            Iterable<Deal> in_vals,
            Reducer<CandleSymbolID, Deal, CandleSymbolID, Deal>.Context context
        ) throws IOException, InterruptedException {
            Iterator<Deal> it = in_vals.iterator();
            check_state(it.hasNext(), "Candle must have at least one deal");

            double close = it.next().get(), open = close, low = close, high = close;
            while (it.hasNext()) {
                close = it.next().get();
                low = Math.min(low, close);
                high = Math.max(high, close);
            }

            // Micro optimizations to reduce the number of sent rows
            double last = open;
            context.write(in_key, new Deal(open));

            // We don't need to send low if it will be sent as open or close
            if (open != low && close != low) {
                last = low;
                context.write(in_key, new Deal(low));
            }
            // We don't need to send high if it will be sent as open or close
            if (open != high && close != high) {
                last = low;
                context.write(in_key, new Deal(high));
            }
            // We don't need to send close if it is the last value that was sent
            if (last != close) {
                context.write(in_key, new Deal(close));
            }
        }
    }
    // endregion

    // region Reducer
    public static class CandleDealReducer extends Reducer<CandleSymbolID, Deal, Text, Text> {
        public MultipleOutputs<Text, Text> outputs;

        @Override
        protected void setup(
            Reducer<CandleSymbolID, Deal, Text, Text>.Context context
        ) throws IOException, InterruptedException {
            super.setup(context);
            init_state(context.getConfiguration());
            outputs = new MultipleOutputs<>(context);
        }

        @Override
        protected void cleanup(
            Reducer<CandleSymbolID, Deal, Text, Text>.Context context
        ) throws IOException, InterruptedException {
            outputs.close();
            super.cleanup(context);
        }

        @Override
        protected void reduce(
            CandleSymbolID in_key,
            Iterable<Deal> in_vals,
            Reducer<CandleSymbolID, Deal, Text, Text>.Context context
        ) throws IOException, InterruptedException {
            Iterator<Deal> it = in_vals.iterator();
            check_state(it.hasNext(), "Candle must have at least one deal");

            double close = it.next().get(), open = close, low = close, high = close;
            while (it.hasNext()) {
                close = it.next().get();
                low = Math.min(low, close);
                high = Math.max(high, close);
            }

            String symbol = in_key.symbol.toString();
            String datetime = long_to_datetime_str(candle_width * in_key.candle.get());

            String open_str = double_to_price_str(open);
            String high_str = double_to_price_str(high);
            String low_str = double_to_price_str(low);
            String close_str = double_to_price_str(close);
            outputs.write(
                new Text(symbol + "," + datetime),
                new Text(open_str + "," + high_str + "," + low_str + "," + close_str),
                symbol
            );
        }
    }
    // endregion

    // region Main
    public static void main(String[] args) throws Exception {
        // region Parse and check arguments
        long start_time = System.nanoTime();
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        conf.set("mapreduce.output.textoutputformat.separator", ",");
        Path input_path = new Path(otherArgs[0]);
        Path output_path = new Path(otherArgs[1]);
        init_state(conf);
        // endregion

        // region Prepare output directory
        FileSystem fs = FileSystem.get(conf);
        if (fs.exists(output_path) && remove_output)
            fs.delete(output_path, true);
        // endregion

        // region Job Specification
        Job job = Job.getInstance(conf, "japanese_candles");
        job.setJarByClass(candle.class);
        job.setNumReduceTasks(num_reducers);

        // Read CSV strings and convert to <(candle, symbol, id), deal>
        TextInputFormat.addInputPath(job, input_path);
        job.setMapperClass(ParseCSVMapper.class);
        job.setMapOutputKeyClass(CandleSymbolID.class); // (candle, symbol, id)
        job.setMapOutputValueClass(Deal.class); // deal

        // Combine local candles before sending to reducer
        if (use_combiner) {
            job.setCombinerClass(CandleDealCombiner.class);
            job.setCombinerKeyGroupingComparatorClass(CandleSymbolGrouper.class);
        }

        // Group by (candle, symbol) instead of (candle, symbol, id)
        job.setGroupingComparatorClass(CandleSymbolGrouper.class);
        if (force_file_order) {
            // If `candle.force.file.order` is true, force the partitioner to
            // send all the candles for each Symbol to the same reducer.
            // This guarantees, that concatenating all the SYMBOL-r-* files
            // still produces the correct candle ordering.
            job.setPartitionerClass(SymbolPartitioner.class);
        } else {
            job.setPartitionerClass(SymbolCandlePartitioner.class);
        }

        // Reduce to <symbol, (open, high, low, close)>
        job.setReducerClass(CandleDealReducer.class);
        job.setOutputKeyClass(Text.class); // symbol
        job.setOutputValueClass(Text.class); // (open, high, low, close) as CSV
        FileOutputFormat.setOutputPath(job, output_path);
        // endregion

        // region Run, time and exit
        int exit_code = job.waitForCompletion(verbose) ? 0 : 1;
        long end_time = System.nanoTime();
        System.out.println("__TIME_NANO__ = " + (end_time - start_time));
        System.exit(exit_code);
        // endregion
    }
    // endregion
}
