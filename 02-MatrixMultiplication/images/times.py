import numpy as np
from matplotlib import pyplot as plt

g_results = {
    1: {
        "REDUCE_INPUT_RECORDS": 1280000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 1280000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 26880000,
        "REDUCE_SHUFFLE_BYTES": 9373576,
        "REDUCE_INPUT_GROUPS": 1,
        "TIME_NANO": 25703993719,
    },
    2: {
        "REDUCE_INPUT_RECORDS": 2560000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 2560000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 53760000,
        "REDUCE_SHUFFLE_BYTES": 18746995,
        "REDUCE_INPUT_GROUPS": 4,
        "TIME_NANO": 29324543766,
    },
    3: {
        "REDUCE_INPUT_RECORDS": 3840000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 3840000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 80640000,
        "REDUCE_SHUFFLE_BYTES": 28122986,
        "REDUCE_INPUT_GROUPS": 9,
        "TIME_NANO": 32077575652,
    },
    4: {
        "REDUCE_INPUT_RECORDS": 5120000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 5120000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 107520000,
        "REDUCE_SHUFFLE_BYTES": 37486417,
        "REDUCE_INPUT_GROUPS": 16,
        "TIME_NANO": 34894917995,
    },
    6: {
        "REDUCE_INPUT_RECORDS": 7680000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 7680000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 161280000,
        "REDUCE_SHUFFLE_BYTES": 56253867,
        "REDUCE_INPUT_GROUPS": 36,
        "TIME_NANO": 40039952696,
    },
    10: {
        "REDUCE_INPUT_RECORDS": 12800000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 12800000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 268800000,
        "REDUCE_SHUFFLE_BYTES": 93851209,
        "REDUCE_INPUT_GROUPS": 100,
        "TIME_NANO": 56262343511,
    },
    14: {
        "REDUCE_INPUT_RECORDS": 17920000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 17920000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 376320000,
        "REDUCE_SHUFFLE_BYTES": 131386859,
        "REDUCE_INPUT_GROUPS": 196,
        "TIME_NANO": 68990184107,
    },
    21: {
        "REDUCE_INPUT_RECORDS": 26880000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 26880000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 564480000,
        "REDUCE_SHUFFLE_BYTES": 197192679,
        "REDUCE_INPUT_GROUPS": 441,
        "TIME_NANO": 97400553193,
    },
    28: {
        "REDUCE_INPUT_RECORDS": 35840000,
        "REDUCE_OUTPUT_RECORDS": 640000,
        "MAP_INPUT_RECORDS": 1280000,
        "MAP_OUTPUT_RECORDS": 35840000,
        "BYTES_READ": 20768845,
        "MAP_OUTPUT_BYTES": 752640000,
        "REDUCE_SHUFFLE_BYTES": 263256867,
        "REDUCE_INPUT_GROUPS": 784,
        "TIME_NANO": 122598972952,
    },
}
ks = set()
for v in g_results.values():
    ks |= set(v)
ks = sorted(ks)

Gs = np.array(sorted(g_results))
Ms = {k: np.array([g_results[g][k] for g in Gs]) for k in ks}


def groups_vs_linears(fig, ax):
    # Num Groups
    ax.set_xticks(Gs)
    eps = 0.1
    gss = Gs.std()
    ax.set_xlim(Gs.min() - gss * eps, Gs.max() + gss * eps)
    ax.set_xlabel("Num. Groups")

    # Time
    Ts = Ms["TIME_NANO"]
    Ts = Ts / 1e9
    tss = Ts.std()
    ax.set_yticks(Ts.round(1))
    ax.set_yticklabels([f"{t:.1f}s" for t in Ts.round(1)])
    ax.set_ylim(Ts.min() - tss * eps, Ts.max() + tss * eps)
    ax.set_ylabel("Time", color="C0")
    ax.tick_params(axis="y", labelcolor="C0")

    ax.plot(Gs, Ts, zorder=100, label="Execution time", color="C0")
    ax.scatter(Gs, Ts, zorder=100, color="C0")
    ax.grid(zorder=-100, linestyle=":")
    ax_host = ax

    # Records
    ax = ax_host.twinx()
    Ts = Ms["MAP_OUTPUT_RECORDS"]
    assert np.all(Ts == Ms["REDUCE_INPUT_RECORDS"])
    Ts = Ts / 1e6
    tss = Ts.std()
    ax.set_yticks(Ts.round(1))
    ax.set_yticklabels([f"{t:.1f}M" for t in Ts.round(1)])
    ax.set_ylim(Ts.min() - tss * eps, Ts.max() + tss * eps)
    ax.set_ylabel("Records", color="C1")
    ax.tick_params(axis="y", labelcolor="C1")

    ax.plot(Gs, Ts, zorder=100, label="Map output records", color="C1")
    ax.plot(Gs, Ts, zorder=100, label="Reduce input records", color="C1")
    ax.scatter(Gs, Ts, zorder=100, color="C1")
    ax.grid(zorder=-100, linestyle=":")

    # Bytes
    ax = ax_host.twinx()
    ax.spines["right"].set_position(("axes", 1.15))
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
    ax.spines["right"].set_visible(True)
    Ts = Ms["MAP_OUTPUT_BYTES"]
    Ts = Ts / (1024 * 1024)
    tss = Ts.std()
    ty = Ts.round(1).tolist()
    tl = [f"{t:.1f}MB" for t in Ts.round(1)]
    ax.set_ylim(Ts.min() - tss * eps, Ts.max() + tss * eps)
    ax.set_ylabel("Bytes", color="C1")
    ax.tick_params(axis="y", labelcolor="C1")

    ax.plot(Gs, Ts, zorder=100, label="Map output bytes", color="C1")
    ax.scatter(Gs, Ts, zorder=100, color="C1")
    ax.grid(zorder=-100, linestyle=":")

    # Shuffle Bytes
    Ts = Ms["REDUCE_SHUFFLE_BYTES"]
    Ts = Ts / (1024 * 1024)
    # tss = Ts.std()
    ty = ty + Ts.round(1).tolist()
    tl = tl + [f"{t:.1f}MB" for t in Ts.round(1)]
    order = np.argsort(ty)
    ty = np.sort(ty)
    diff = np.diff(ty, prepend=-100) > 10
    tl = [tl[idx] for idx in order if diff[idx]]
    ty = ty[diff]
    ax.set_yticks(ty)
    ax.set_yticklabels(tl)

    ax.plot(
        Gs,
        Ts,
        zorder=100,
        label="Reduce shuffle bytes",
        linestyle="--",
        dashes=(5, 5),
        color="C1",
    )
    ax.scatter(Gs, Ts, zorder=100, color="C1")
    ax.grid(zorder=-100, linestyle=":")

    fig.legend(loc="upper center")


def groups_vs_constants(fig, ax):
    idx = 0
    pos = np.linspace(0, 1, 6)[1:-1][::-1]
    ax.text(
        0.5,
        pos[0],
        "CONSTANTS",
        weight="bold",
        size=42 * 3 // 2,
        ha="center",
        va="center",
    )
    for name, values in Ms.items():
        if not np.all(np.diff(values) == 0):
            continue
        ax.axis("off")

        value = values[0].item()
        if "BYTES" in name:
            value = value / (1024 * 1024)
            value = f"{value:.1f}MB"
        elif "RECORDS" in name:
            value = value / 1e6
            value = f"{value:.1f}M"
        else:
            raise NotImplementedError(name)

        first, *others = name.split("_")
        first = first.capitalize()
        others = [o.lower() for o in others]
        name = " ".join([first] + others)
        name = f"{name}$ = {value}$"
        ax.text(0.5, pos[1 + idx], name, size=42, ha="center", va="center")
        idx += 1


def groups_vs_others(fix, ax):
    # Num Groups
    ax.set_xticks(Gs)
    eps = 0.1
    gss = Gs.std()
    ax.set_xlim(Gs.min() - gss * eps, Gs.max() + gss * eps)
    ax.set_xlabel("Num. Groups")

    # Reduce Input Groups
    Ts = Ms["REDUCE_INPUT_GROUPS"]
    Ts = Ts
    tss = Ts.std()
    ty = Ts.round(1)
    tl = [str(t) for t in Ts.round(1)]
    diff = np.diff(ty, prepend=-100) > 6
    ty = ty[diff]
    tl = [tl[idx] for idx in range(len(tl)) if diff[idx]]
    ax.set_yticks(ty)
    ax.set_yticklabels(tl)
    ax.set_ylim(Ts.min() - tss * eps, Ts.max() + tss * eps)
    ax.set_ylabel("Reduce Groups", color="C2")
    ax.tick_params(axis="y", labelcolor="C2")

    ax.plot(Gs, Ts, zorder=100, label="Reduce input groups", color="C2")
    ax.scatter(Gs, Ts, zorder=100, color="C2")
    ax.grid(zorder=-100, linestyle=":")

    fig.legend(loc="upper center")


scale = 1.25
for plot_fn in [groups_vs_linears, groups_vs_constants, groups_vs_others]:
    fig, ax = plt.subplots(
        figsize=(14.4 / scale, 8.1 / scale), dpi=100 * scale, tight_layout=True
    )
    plot_fn(fig, ax)
    fig.savefig(f"{plot_fn.__name__}.png")
