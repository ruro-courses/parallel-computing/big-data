#### Компиляция программы

```sh
mvn clean package
```

#### Запуск программы

Например, вот так:
```sh
hadoop jar target/mm.jar mm \
    -Dmm.remove.output=true \
    -Dmm.verbose=true \
    -Dmm.groups=1 \
    -Dmm.tags="ABC" \
    -Dmm.float-format="%.3f" \
    -Dmapred.reduce.tasks=1 \
    data/test_numpy/A \
    data/test_numpy/B \
    output
```

#### Отчет

[Ссылка](https://docs.google.com/document/d/1pXRBppPVynTUiMFQa4LXGkhiOgxOy_uZFIQKZ22U1sA/edit?usp=sharing)
