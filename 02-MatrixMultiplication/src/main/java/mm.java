import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormatCounter;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

// region WritableComparable container types
class SliceLeftRightSideOuterInner implements WritableComparable<SliceLeftRightSideOuterInner> {
    public final IntWritable slice_left = new IntWritable();
    public final IntWritable slice_right = new IntWritable();
    public final BooleanWritable side = new BooleanWritable();
    public final IntWritable outer = new IntWritable();
    public final IntWritable inner = new IntWritable();

    @SuppressWarnings("unused") // used during hadoop deserialization
    public SliceLeftRightSideOuterInner() {}

    public SliceLeftRightSideOuterInner(int slice_left_, int slice_right_, boolean side_, int outer_, int inner_) {set(slice_left_, slice_right_, side_, outer_, inner_);}

    public void set(int slice_left_, int slice_right_, boolean side_, int outer_, int inner_) {
        slice_left.set(slice_left_);
        slice_right.set(slice_right_);
        side.set(side_);
        outer.set(outer_);
        inner.set(inner_);
    }

    @Override
    public int compareTo(SliceLeftRightSideOuterInner other) {
        int r;
        if ((r = slice_left.compareTo(other.slice_left)) != 0) return r;
        if ((r = slice_right.compareTo(other.slice_right)) != 0) return r;
        if ((r = side.compareTo(other.side)) != 0) return r;
        if ((r = outer.compareTo(other.outer)) != 0) return r;
        if ((r = inner.compareTo(other.inner)) != 0) return r;
        return 0;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        slice_left.write(out);
        slice_right.write(out);
        side.write(out);
        outer.write(out);
        inner.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        slice_left.readFields(in);
        slice_right.readFields(in);
        side.readFields(in);
        outer.readFields(in);
        inner.readFields(in);
    }
}

class Value extends FloatWritable {
    @SuppressWarnings("unused") // used during hadoop deserialization
    public Value() {}

    public Value(float f) {super(f);}
}
// endregion

public class mm {
    // region Constants
    public static final boolean CHECK = true; // setting this to false disables sanity checks
    private static final Path data_path = new Path(Path.SEPARATOR + "data");
    private static final Path shape_path = new Path(Path.SEPARATOR + "size");
    // endregion

    // region Globals
    private static int num_reducers;
    private static boolean verbose;
    private static boolean remove_output;
    private static String float_format;

    private static byte tag_left;
    private static byte tag_right;
    private static byte tag_output;

    private static int size_left;
    private static int size_inner;
    private static int size_right;

    private static int num_slices_left;
    private static int num_slices_right;

    private static int slice_size_left;
    private static int slice_size_right;
    private static int last_slice_size_left;
    private static int last_slice_size_right;
    // endregion

    // region Helper Functions
    public static void assert_(boolean ok, String message) {
        if (!ok) throw new IllegalStateException(message);
    }

    public static int ceil_div(int a, int b) {return (a + b - 1) / b;}

    public static String float_to_value_str(float price) {
        return String.format(float_format, price);
    }

    public static int[] read_shape(FileSystem fs, Path path) {
        try (DataInputStream shape_stream = fs.open(Path.mergePaths(path, shape_path))) {
            Scanner shape_scanner = new Scanner(shape_stream);
            int i = shape_scanner.nextInt();
            int j = shape_scanner.nextInt();
            if (CHECK)
                assert_(!shape_scanner.hasNext(), "Unexpected continuation in size file for " + path);
            return new int[]{i, j};
        } catch (IOException e) {
            throw new IllegalStateException("Couldn't read the size for " + path, e);
        }
    }

    public static void write_shape(FileSystem fs, Path path, int[] shape) {
        try (DataOutputStream shape_stream = fs.create(Path.mergePaths(path, shape_path))) {
            PrintWriter shape_writer = new PrintWriter(shape_stream);
            shape_writer.printf("%d\t%d\n", shape[0], shape[1]);
            shape_writer.flush();
        } catch (IOException e) {
            throw new IllegalStateException("Couldn't write the size for " + path, e);
        }
    }
    // endregion


    // region Initialize global state from Configuration
    public static int infer_slice_size(int num, int size, boolean allow_impossible) {
        int slice = ceil_div(size, num);
        if (slice * (num - 1) < size) return slice;
        // The requested slice number is impossible with any slice size.
        // Using the computed slice size will result in less slices than originally requested.
        if (allow_impossible) return slice;
        throw new IllegalStateException("It is impossible to split " + size + " into " + num + " equally sized chunks");
    }

    public static void init_state(Configuration conf) {
        // region Boolean options
        verbose = conf.getBoolean("mm.verbose", true);
        remove_output = conf.getBoolean("mm.remove.output", true);
        boolean allow_impossible_slices = conf.getBoolean("mm.groups.allow_impossible", false);
        // endregion

        // region Tags
        byte[] tags = conf.get("mm.tags", "ABC").getBytes(StandardCharsets.US_ASCII);
        if (CHECK)
            assert_(tags.length == 3, "`mm.tags` must be a 3 letter ASCII string");

        tag_left = tags[0];
        tag_right = tags[1];
        tag_output = tags[2];
        // endregion

        // region Sizes
        size_left = conf.getInt("mm._internal.size.left", -1);
        size_inner = conf.getInt("mm._internal.size.inner", -1);
        size_right = conf.getInt("mm._internal.size.right", -1);
        if (CHECK)
            assert_(
                size_left != -1 && size_inner != -1 && size_right != -1,
                "Couldn't fetch matrix sizes"
            );
        // endregion

        // region Slices
        int num_slices = conf.getInt("mm.groups", 1);
        num_slices_left = conf.getInt("mm.groups.left", num_slices);
        num_slices_right = conf.getInt("mm.groups.right", num_slices);

        // "0" slices means the same number of slices as rows/columns (1 row/column per slice)
        num_slices_left = num_slices_left == 0 ? size_left : num_slices_left;
        num_slices_right = num_slices_right == 0 ? size_right : num_slices_right;
        if (CHECK)
            assert_(0 < num_slices_left && num_slices_left <= size_left, "Requested invalid `mm.groups.left` size");
        if (CHECK)
            assert_(0 < num_slices_right && num_slices_right <= size_right, "Requested invalid `mm.groups.right` size");

        // Try to infer the slice sizes based on requested amount of slices
        slice_size_left = infer_slice_size(num_slices_left, size_left, allow_impossible_slices);
        slice_size_right = infer_slice_size(num_slices_right, size_right, allow_impossible_slices);
        int actual_slices_left = ceil_div(size_left, slice_size_left);
        int actual_slices_right = ceil_div(size_right, slice_size_right);
        if (allow_impossible_slices) {
            num_slices_left = actual_slices_left;
            num_slices_right = actual_slices_right;
        } else {
            if (CHECK)
                assert_(num_slices_left == actual_slices_left, "An impossible slice number was requested");
            if (CHECK)
                assert_(num_slices_right == actual_slices_right, "An impossible slice number was requested");
        }
        last_slice_size_left = 1 + (size_left - 1) % slice_size_left;
        last_slice_size_right = 1 + (size_right - 1) % slice_size_right;
        // endregion

        // region Float format and Number of reducers
        float_format = conf.get("mm.float-format", "%.3f");
        num_reducers = conf.getInt("mapred.reduce.tasks", 1);
        if (CHECK)
            assert_(num_reducers >= 1, "`num.reducers` must be at least 1");
        // endregion
    }
    // endregion

    // region Mapper
    public static class BroadcastMapper extends Mapper<Object, Text, SliceLeftRightSideOuterInner, Value> {
        @Override
        protected void setup(
            Mapper<Object, Text, SliceLeftRightSideOuterInner, Value>.Context context
        ) throws IOException, InterruptedException {
            super.setup(context);
            init_state(context.getConfiguration());
        }

        @Override
        public void map(
            Object in_key,
            Text in_val,
            Context context
        ) throws IOException, InterruptedException {
            String[] array = in_val.toString().split("\t");
            if (CHECK)
                assert_(array.length == 4, "Input data has wrong number of columns");

            // Extract tag
            byte[] tag = array[0].getBytes(StandardCharsets.US_ASCII);
            if (CHECK)
                assert_(tag.length == 1, "Tag must be a single ASCII character");

            boolean side;
            if (tag[0] == tag_left)
                side = true;
            else if (tag[0] == tag_right)
                side = false;
            else
                throw new IllegalStateException("Invalid input tag " + tag[0]);

            int i = Integer.parseInt(array[1]);
            int j = Integer.parseInt(array[2]);
            int inner, outer, slice_left, slice_right, num_broadcast;
            if (side) {
                // Left
                if (CHECK)
                    assert_(
                        0 <= i && i < size_left &&
                            0 <= j && j < size_inner,
                        "Invalid index for given matrix size"
                    );

                outer = i;
                inner = j;
                slice_left = outer / slice_size_left;
                slice_right = -1;
                outer = outer % slice_size_left;
                num_broadcast = num_slices_right;
            } else {
                // Right
                if (CHECK)
                    assert_(
                        0 <= i && i < size_inner &&
                            0 <= j && j < size_right,
                        "Invalid index for given matrix size"
                    );

                inner = i;
                outer = j;
                slice_left = -1;
                slice_right = outer / slice_size_right;
                outer = outer % slice_size_right;
                num_broadcast = num_slices_left;
            }

            SliceLeftRightSideOuterInner key = new SliceLeftRightSideOuterInner(slice_left, slice_right, side, outer, inner);
            Value value = new Value(Float.parseFloat(array[3]));

            for (int broadcast = 0; broadcast < num_broadcast; ++broadcast) {
                if (side) // Left
                    key.slice_right.set(broadcast);
                else // Right
                    key.slice_left.set(broadcast);
                context.write(key, value);
            }
        }
    }
    // endregion

    // region Partitioner and Grouper (Secondary Key Comparisons)
    public static class SlicePartitioner extends Partitioner<SliceLeftRightSideOuterInner, Value> {
        @Override
        public int getPartition(
            SliceLeftRightSideOuterInner in_key,
            Value in_val,
            int num_partitions
        ) {
            // Only partition by left and right slice indices
            int slice_left = in_key.slice_left.get();
            int slice_right = in_key.slice_right.get();
            return (num_slices_right * slice_left + slice_right) % num_partitions;
        }
    }

    public static class SliceGrouper extends WritableComparator {
        public SliceGrouper() {super(SliceLeftRightSideOuterInner.class, true);}

        @Override
        public int compare(WritableComparable a_, WritableComparable b_) {
            SliceLeftRightSideOuterInner a = (SliceLeftRightSideOuterInner) a_;
            SliceLeftRightSideOuterInner b = (SliceLeftRightSideOuterInner) b_;

            // Only group by left and right slice indices
            int r;
            if ((r = a.slice_left.compareTo(b.slice_left)) != 0) return r;
            if ((r = a.slice_right.compareTo(b.slice_right)) != 0) return r;
            return 0;
        }
    }
    // endregion

    // region Reducer
    public static class EinSumReducer extends Reducer<SliceLeftRightSideOuterInner, Value, Text, Text> {
        @Override
        protected void setup(
            Reducer<SliceLeftRightSideOuterInner, Value, Text, Text>.Context context
        ) throws IOException, InterruptedException {
            super.setup(context);
            init_state(context.getConfiguration());
        }

        public String make_element(int i, int j, float value) {
            return "" + i + '\t' + j + '\t' + float_to_value_str(value);
        }

        @Override
        protected void reduce(
            SliceLeftRightSideOuterInner in_key,
            Iterable<Value> in_vals,
            Reducer<SliceLeftRightSideOuterInner, Value, Text, Text>.Context context
        ) throws IOException, InterruptedException {
            // Slice indices for the current slice
            int slice_left = in_key.slice_left.get();
            int slice_right = in_key.slice_right.get();
            // Actual slice sizes for the current slice
            int slice_size_left_ = slice_left == num_slices_left - 1 ? last_slice_size_left : slice_size_left;
            int slice_size_right_ = slice_right == num_slices_right - 1 ? last_slice_size_right : slice_size_right;

            // Save the slices to memory
            float[] left_buffer = new float[slice_size_left_ * size_inner];
            float[] right_buffer = new float[slice_size_right_ * size_inner];
            for (Value val : in_vals) {
                boolean side = in_key.side.get();
                int inner = in_key.inner.get();
                int outer = in_key.outer.get();
                if (side)
                    left_buffer[outer * size_inner + inner] = val.get();
                else
                    right_buffer[outer * size_inner + inner] = val.get();
            }

            // Do matrix multiplication
            Text tag = new Text(new String(new byte[]{tag_output}, StandardCharsets.US_ASCII));
            Text output = new Text();
            int slice_offset_left = slice_left * slice_size_left;
            int slice_offset_right = slice_right * slice_size_right;
            for (int left = 0; left < slice_size_left_; ++left)
                for (int right = 0; right < slice_size_right_; ++right) {
                    // Compute the inner product
                    int left_offset = left * size_inner;
                    int right_offset = right * size_inner;
                    double value = 0; // Reduce in double instead of float to avoid cumulative errors
                    for (int inner = 0; inner < size_inner; ++inner) {
                        double l = left_buffer[left_offset + inner];
                        double r = right_buffer[right_offset + inner];
                        value += l * r;
                    }
                    float value_f = (float) value;

                    // Dump the output element
                    if (value_f != 0.0f) {
                        output.set(make_element(slice_offset_left + left, slice_offset_right + right, value_f));
                        context.write(tag, output);
                    }
                }
        }
    }
    // endregion

    // region Main
    public static void main(String[] args) throws Exception {
        // region Parse and check arguments
        long start_time = System.nanoTime();
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        Path input_path_left = new Path(otherArgs[0]);
        Path input_path_right = new Path(otherArgs[1]);
        Path output_path = new Path(otherArgs[2]);
        // endregion

        // region Read matrix shapes and init state
        FileSystem fs = FileSystem.get(conf);
        int[] shape_left = read_shape(fs, input_path_left);
        int[] shape_right = read_shape(fs, input_path_right);
        if (CHECK)
            assert_(
                shape_left[1] == shape_right[0],
                "Inner dimension of the left and right matrices doesn't match: " +
                    Arrays.toString(shape_left) + " @ " +
                    Arrays.toString(shape_right)
            );
        conf.setInt("mm._internal.size.left", shape_left[0]);
        conf.setInt("mm._internal.size.inner", shape_left[1]);
        conf.setInt("mm._internal.size.right", shape_right[1]);
        init_state(conf);
        // endregion

        // region Prepare output directory
        if (fs.exists(output_path) && remove_output)
            fs.delete(output_path, true);
        write_shape(fs, output_path, new int[]{shape_left[0], shape_right[1]});
        // endregion

        // region Job Specification
        Job job = Job.getInstance(conf, "matrix_multiplication");
        job.setJarByClass(mm.class);
        job.setNumReduceTasks(num_reducers);

        // Read TSV strings
        TextInputFormat.addInputPath(job, Path.mergePaths(input_path_left, data_path));
        TextInputFormat.addInputPath(job, Path.mergePaths(input_path_right, data_path));

        // Convert to <(left, right, side, outer, inner), value>, broadcast across groups
        job.setMapperClass(BroadcastMapper.class);
        job.setMapOutputKeyClass(SliceLeftRightSideOuterInner.class);
        job.setMapOutputValueClass(Value.class);

        // Group by (left, right)
        job.setGroupingComparatorClass(SliceGrouper.class);
        job.setPartitionerClass(SlicePartitioner.class);

        // Reduce by computing the einsum/inner product
        job.setReducerClass(EinSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileOutputFormat.setOutputPath(job, Path.mergePaths(output_path, data_path));
        // endregion

        // region Run, time and exit
        int exit_code = job.waitForCompletion(verbose) ? 0 : 1;
        long end_time = System.nanoTime();

        // Counters
        Counters counters = job.getCounters();
        HashMap<String, Counter> counter_map = new HashMap<>();
        for (TaskCounter counter : new TaskCounter[]{
            TaskCounter.MAP_INPUT_RECORDS,
            TaskCounter.MAP_OUTPUT_RECORDS,
            // TaskCounter.MAP_INPUT_BYTES -> FileInputFormatCounter.BYTES_READ,
            TaskCounter.MAP_OUTPUT_BYTES,
            TaskCounter.REDUCE_INPUT_GROUPS,
            TaskCounter.REDUCE_SHUFFLE_BYTES,
            TaskCounter.REDUCE_INPUT_RECORDS,
            TaskCounter.REDUCE_OUTPUT_RECORDS
        })
            counter_map.put(counter.toString(), counters.findCounter(counter));
        counter_map.put(FileInputFormatCounter.BYTES_READ.toString(), counters.findCounter(FileInputFormatCounter.BYTES_READ));

        // Print results
        StringBuilder results = new StringBuilder();
        results.append("__RESULTS__ = {");
        for (String name : counter_map.keySet()) {
            results.append('"');
            results.append(name);
            results.append("\": ");
            results.append(counter_map.get(name).getValue());
            results.append(", ");
        }
        results.append("\"TIME_NANO\": ");
        results.append(end_time - start_time);
        results.append('}');
        System.out.println(results);
        System.exit(exit_code);
        // endregion
    }
    // endregion
}
